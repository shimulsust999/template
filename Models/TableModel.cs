﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Testing.Models
{
    public class TableModel
    {
        public int Id { get; set; }
        public string Name { get; set;}
        public int Age { get; set; }
        public string Area { get; set; }
        public string Company { get; set; }

        public string Role { get; set; }

        public string Type { get; set; }



    }
}
