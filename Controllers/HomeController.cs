﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Testing.Models;

namespace Testing.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            var data=new TableModel[]{
                new TableModel{Name="Karim",Age=22,Area="Dhaka",Company="BS-23",Role="Software Engineer.",Type="abcd" },
                new TableModel{Name="Rahim",Age=22,Area="Dhaka",Company="BS-23",Role="Software Engineer.",Type="abcd" },
                new TableModel{Name="Abdul",Age=22,Area="Dhaka",Company="BS-23",Role="Software Engineer.",Type="abcd" },
                new TableModel{Name="Shifat",Age=22,Area="Dhaka",Company="BS-23",Role="Software Engineer.",Type="abcd" },
            };
            return View(data);
        }

        //[HttpPost]
        //[ActionName("Index")]
        //public async Task<IActionResult> Download()
        //{
        //    await 

        //}

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
